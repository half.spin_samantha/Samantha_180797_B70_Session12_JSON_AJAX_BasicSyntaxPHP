/**
 * Created by Web App Develop-PHP on 8/20/2017.
 */


var myRequest = new XMLHttpRequest();

myRequest.open("GET","https://learnwebcode.github.io/json-example/animals-1.json");
myRequest.onload = function () {

   animals = JSON.parse(myRequest.responseText);

    document.write(myData);
};

myRequest.send();


var i=0;

$(document).ready(function (){

    $("#nextBTN").click(function () {
        if(i==animals.length-1) {
            $("#nextBTN").hide();
            $("#prevBTN").show();
        }
        else  $("#nextBTN").show();

        display();
        i++;
    });



    $("#prevBTN").click(function () {
        i--;
        display();

        if(i<=0) {
            $("#nextBTN").show();
            $("#prevBTN").hide();
        }
        else  $("#prevBTN").show();

        display();
        i++;
    });


});
function display() {

    myContent = "Name: " + animals[i].name + "<br>";
    myContent += "Species: " + animals[i].species + "<br>";

    myContent += "Likes: " + animals[i].foods.likes ;
    myContent += "&nbsp&nbsp&nbsp&nbsp";

    myContent += "DisLikes: " + animals[i].foods.dislikes + "<br>";

    i++;

    $("#myDIV").html(myContent);

    $("#myDIV").css("background", "GREEN");


}
