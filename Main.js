/**
 * Created by Web App Develop-PHP on 8/20/2017.
 */

    var cat = {
        "name" : "MeowsALot",
        "species" : "cat",
        "foods" : {
            "likes" : [ "tuna","milk" , "mouse"],
            
            "dislikes" : [ "grass" , "dog's food" , "vegetables"]
        }
};

// defining values inside objects
// document.write("I'm " + cat.name + "!" + "I'm a" + cat.species + "<br>I like to eat " + cat.foods.likes + " and I don't like to eat " + cat.foods.dislikes);

// for(var i=0;i<3;i++) {
//     document.write(cat + "<br");
// }

//https://learnwebcode.github.io/json-example/animals-1.json
// defining objects inside arrays

var animals = [
    {
    "name" : "MeowsALot",
    "species" : "cat",
    "foods" : {
        "likes": ["tuna", "milk", "mouse"],

        "dislikes": ["grass", "dog's food", "vegetables"]}
    },

    {"name" : "BarksALot",
        "species" : "dog",
        "foods" : {
            "likes" : [ "meat","bones" , "Dog's food"],

            "dislikes" : [ "grass" , "cat's food" , "fruits"]}
    },

    {
        "name": "ChirpingALot",
        "species": "bird",
        "foods": {
            "likes": ["grass", "grains", "fruits"],

            "dislikes": ["meat", "fish"]
        }
    }

];

var i=0;

$(document).ready(function (){

            $("#nextBTN").click(function () {
                if(i==animals.length-1) {
                    $("#nextBTN").hide();
                    $("#prevBTN").show();
                }
                else  $("#nextBTN").show();

                display();
                i++;
            });



    $("#prevBTN").click(function () {
        i--;
        display();

        if(i<=0) {
            $("#nextBTN").show();
            $("#prevBTN").hide();
        }
        else  $("#prevBTN").show();

        display();
        i++;
    });


});
    function display() {

            myContent = "Name: " + animals[i].name + "<br>";
            myContent += "Species: " + animals[i].species + "<br>";

            myContent += "Likes: " + animals[i].foods.likes ;
            myContent += "&nbsp&nbsp&nbsp&nbsp";

            myContent += "DisLikes: " + animals[i].foods.dislikes + "<br>";

             i++;

        $("#myDIV").html(myContent);

        $("#myDIV").css("background", "GREEN");


    }
